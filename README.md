# "Automate the boring stuff with Python"

This repository holds the files I create while working through
[Automate the boring stuff with Python][1].

[1]: https://automatetheboringstuff.com/
