## "hello, world" program that also asks for the user's name

print( "hello, world" )
print( "what is your name?" )  ## ask for user's name

myName = input()

print()  ## Print empty line
print( "It is good to meet you, " + myName +"." )
print()
print( "The length of your name is: " )
print( len( myName ) )

print()
print( "What is your age? " )  ## ask for user's age

myAge = input()

print()
print( "you will be " + str( int( myAge ) + 1 ) + " in a year." )
