>>> True and True
True
>>> True and False
False
>>> False and False
False
>>> False and True
False
>>> 
>>> True or True
True
>>> True or False
True
>>> False or True
True
>>> False or False
False
>>> 
>>> not True
False
>>> not False
True
>>> not not True
True
>>> not not False
False
>>> not (not True and True)
True
>>> 
>>> ( 4 < 5 ) and ( 5 < 6 )
True
>>> ( 4 < 5 ) and ( 9 < 5 )
False
>>> ( 1 == 2 ) or ( 2 == 2 )
True
>>> 
>>> 2 + 2 == 4 and not 2 + 2 == 5 and 2 * 2 == 2 + 2
True
>>> ( 2 + 2 == 4 ) and not ( 2 + 2 == 5 ) and ( 2 * 2 == 2 + 2 )
True
>>> 
