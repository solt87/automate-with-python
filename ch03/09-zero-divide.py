def spam( divideBy ):
    try:
        return 42 / divideBy
    except ZeroDivisionError:
        print( "Error: invalid argument" )

print( spam( 21 ) )
print( spam( 12 ) )
print( spam( 0 ) )
print( spam( 1 ) )