import sys

def collatz( number ):
    if number % 2 == 0:
        result = number // 2
        print( result )
    else:
        result = number * 3 + 1
        print( result )
    
    return result

number = 0  ## initial value


print( "Please enter a positive integer." )
try:
    number = int( input() )
except ValueError:
        print( "Value given cannot be converted to integer." )
        sys.exit()

while number != 1:
    number = collatz( number )
