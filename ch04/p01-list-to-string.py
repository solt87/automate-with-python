def listToString( lst ):
    result = ""
    
    for element in lst[ :-1 ]:
        result += str( element) + ", "
    
    result += " and " + str( lst[ -1 ] )
    
    return result

spam = [ "busta", "rasta", "blasta", "fasta" ]
print( spam )
print( listToString( spam ) )

eggs = [ 1, 2, 3, 4, 5, 6, 7 ]
print( eggs )
print( listToString( eggs ) )