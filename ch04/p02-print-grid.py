def printGrid( grd ):
    
    for col in range( len( grd[ 0 ] ) ):
        for row in range( len( grd ) - 1, 0, -1 ):
            print( grd[ row ][ col ], end="" )
        print( "" )
    
    return None

grid = [['.', '.', '.', '.', '.', '.'],
        ['.', 'O', 'O', '.', '.', '.'],
        ['O', 'O', 'O', 'O', '.', '.'],
        ['O', 'O', 'O', 'O', 'O', '.'],
        ['.', 'O', 'O', 'O', 'O', 'O'],
        ['O', 'O', 'O', 'O', 'O', '.'],
        ['O', 'O', 'O', 'O', '.', '.'],
        ['.', 'O', 'O', '.', '.', '.'],
        ['.', '.', '.', '.', '.', '.']]

printGrid( grid )