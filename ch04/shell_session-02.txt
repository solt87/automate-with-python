>>> [ 1, 2, 3 ] + [ "A", "B", "C" ]
[1, 2, 3, 'A', 'B', 'C']
>>> [ "X", "Y", "Z" ] * 3
['X', 'Y', 'Z', 'X', 'Y', 'Z', 'X', 'Y', 'Z']
>>> spam = [ 1, 2, 3 ]
>>> spam = spam + [ "A", "B", "C" ]
>>> spam
[1, 2, 3, 'A', 'B', 'C']
>>> spam =  [ "yo", "ho", "sho", "blow" ]
>>> spam
['yo', 'ho', 'sho', 'blow']
>>> del spam[ 2 ]
>>> spam
['yo', 'ho', 'blow']
>>> del spam[ 2 ]
>>> spam
['yo', 'ho']
>>> 
>>> 
>>> 
>>> 
