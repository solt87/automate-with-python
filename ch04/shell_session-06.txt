Python 3.6.4
>>> spam = [ "sup", "yo", "hi", "whadup" ]
>>> spam.index( "hi" )
2
>>> spam.index( "sup" )
0
>>> spam.index( "yo" )
1
>>> spam.index( "whadup" )
3
>>> spam.index( "Good morning." )
Traceback (most recent call last):
  File "<pyshell>", line 1, in <module>
ValueError: 'Good morning.' is not in list
>>> 
>>> spam[ 1 ] = "sup"
>>> spam
['sup', 'sup', 'hi', 'whadup']
>>> spam-index( "sup" )
Traceback (most recent call last):
  File "<pyshell>", line 1, in <module>
NameError: name 'index' is not defined
>>> spam.index( "sup" )
0
>>> spam.index( "hi" )
2
>>> 
>>> 
>>> spam = [ "fish", "dog", "bat" ]
>>> spam
['fish', 'dog', 'bat']
>>> spam.append( "mouse" )
>>> spam
['fish', 'dog', 'bat', 'mouse']
>>> 
>>> spam = [ "fish", "dog", "bat" ]
>>> spam
['fish', 'dog', 'bat']
>>> spam.insert( 1, "chickin" )
>>> spam
['fish', 'chickin', 'dog', 'bat']
>>> 
>>> eggs = "hello"
>>> eggs.append( "hi" )
Traceback (most recent call last):
  File "<pyshell>", line 1, in <module>
AttributeError: 'str' object has no attribute 'append'
>>> type( eggs )
<class 'str'>
>>> bacon = 42
>>> bacon.insert( 0, 77 )
Traceback (most recent call last):
  File "<pyshell>", line 1, in <module>
AttributeError: 'int' object has no attribute 'insert'
>>> type( bacon )
<class 'int'>
>>> type( type( bacon ) )
<class 'type'>
>>> 
>>> 
>>> spam = [ "fish", "bat", "rat", "elephant" ]
>>> spam
['fish', 'bat', 'rat', 'elephant']
>>> spam.remove( "bat" )
>>> spam
['fish', 'rat', 'elephant']
>>> spam.insert( 1, "rat" )
>>> soam
Traceback (most recent call last):
  File "<pyshell>", line 1, in <module>
NameError: name 'soam' is not defined
>>> spam
['fish', 'rat', 'rat', 'elephant']
>>> spam.remove( "rat" )
>>> spam
['fish', 'rat', 'elephant']
>>> spam.insert( 1, "bat" )
>>> spam
['fish', 'bat', 'rat', 'elephant']
>>> spam.insert( 4, "baboon" )
>>> spam
['fish', 'bat', 'rat', 'elephant', 'baboon']
>>> spam.remove( "chickin" )
Traceback (most recent call last):
  File "<pyshell>", line 1, in <module>
ValueError: list.remove(x): x not in list
>>> 
>>> spam = [ 1, 2, 1, 3, 4, 5, 1, 6, 1 ]
>>> spam
[1, 2, 1, 3, 4, 5, 1, 6, 1]
>>> spam.remove( 1 )
>>> spam
[2, 1, 3, 4, 5, 1, 6, 1]
>>> spam.index ( 1 )
1
>>> del( spam[ 1 ] )
>>> spam
[2, 3, 4, 5, 1, 6, 1]
>>> spam.index( 1 )
4
>>> 
>>> 
>>> 
>>> spam = [ 2, 5, 3.14, 1, -7 ]
>>> spam
[2, 5, 3.14, 1, -7]
>>> spam.sort()
>>> spam
[-7, 1, 2, 3.14, 5]
>>> spam = [ "antz", "fishes", "chickins", "dogs", "elephants", "badgers" ]
>>> spam
['antz', 'fishes', 'chickins', 'dogs', 'elephants', 'badgers']
>>> spam.sort()
>>> spam
['antz', 'badgers', 'chickins', 'dogs', 'elephants', 'fishes']
>>> spam.sort( reverse = True )
>>> spam
['fishes', 'elephants', 'dogs', 'chickins', 'badgers', 'antz']
>>> spam.sort(9
              )
Traceback (most recent call last):
  File "<pyshell>", line 1, in <module>
TypeError: must use keyword argument for key function
>>> spam.sort()
>>> spam
['antz', 'badgers', 'chickins', 'dogs', 'elephants', 'fishes']
>>> spam.sort( reverse = True )
>>> spam
['fishes', 'elephants', 'dogs', 'chickins', 'badgers', 'antz']
>>> 
>>> 
>>> spam = [ 2, 1, "Bob", "Eve", "Alice", 0 ]
>>> spam
[2, 1, 'Bob', 'Eve', 'Alice', 0]
>>> spam.sort()
Traceback (most recent call last):
  File "<pyshell>", line 1, in <module>
TypeError: '<' not supported between instances of 'str' and 'int'
>>> 
>>> spam = [ "Alice", "ants", "Bob", "badgers", "Carol", "cats" ]
>>> spam
['Alice', 'ants', 'Bob', 'badgers', 'Carol', 'cats']
>>> spam.sort()
>>> spam
['Alice', 'Bob', 'Carol', 'ants', 'badgers', 'cats']
>>> 
>>> spam = [ "a", "z", "A", "Z" ]
>>> spam
['a', 'z', 'A', 'Z']
>>> spam.sort(  key=str.lower ))
  File "<pyshell>", line 1
    spam.sort(  key=str.lower ))
                               ^
SyntaxError: invalid syntax

>>> spam.sort(  key=str.lower )
>>> spam
['a', 'A', 'z', 'Z']
>>> spam = [ "Alice", "ants", "Bob", "badgers", "Carol", "cats" ]
>>> spam
['Alice', 'ants', 'Bob', 'badgers', 'Carol', 'cats']
>>> spam.sort(  key=str.lower )
>>> spam
['Alice', 'ants', 'badgers', 'Bob', 'Carol', 'cats']
>>> 
