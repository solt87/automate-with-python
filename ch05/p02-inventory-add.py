## Inventory updater

def displayInventory( inventory ):
    print( "Inventory:" )
    
    item_total = 0
    
    for k, v in inventory.items():
        print( str( v ) +" " + k )
        item_total += v
    
    print( "\nTotal number of items: " + str( item_total ) )

def addToInventory( inventory, addedItems ):
    
    result_inv = inventory.copy()
    
    for item in addedItems:
        result_inv.setdefault( item, 0 )
        result_inv[ item ] += 1
    
    return result_inv

inv = {'gold coin': 42, 'rope': 1}
dragonLoot = ['gold coin', 'dagger', 'gold coin', 'gold coin', 'ruby']
inv = addToInventory( inv, dragonLoot )
displayInventory( inv )