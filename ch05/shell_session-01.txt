>>> myChickin = { "size": "fat", "color": "white", "dispositio": "loud" }
>>> myChickin
{'size': 'fat', 'color': 'white', 'dispositio': 'loud'}
>>> myChickin[ "color" }
  File "<pyshell>", line 1
    myChickin[ "color" }
                       ^
SyntaxError: invalid syntax

>>> myChickin[ "color" ]
'white'
>>> myChickin[ "size" ]
'fat'
>>> myChickin[ "disposition" ]
Traceback (most recent call last):
  File "<pyshell>", line 1, in <module>
KeyError: 'disposition'
>>> myChickin[ "dispositio" ]
'loud'
>>> del( myChickin[ "dispositio" ] )
>>> myChickin
{'size': 'fat', 'color': 'white'}
>>> myChickin[ "disposition" ] = "loud"
>>> myChickin
{'size': 'fat', 'color': 'white', 'disposition': 'loud'}
>>> type( myChickin )
<class 'dict'>
>>> "My chickin has " + myChickin[ "color" ] + " feathers."
'My chickin has white feathers.'
>>> 
>>> 
>>> spam = { 12345: "Luggage Combination", 42: "The Answer" }
>>> spam
{12345: 'Luggage Combination', 42: 'The Answer'}
>>> spam[ 12345 ]
'Luggage Combination'
>>> spam[ 42 ]
'The Answer'
>>> 
>>> 
>>> 
>>> spam = [ "fish", "dog", "moose" ]
>>> bacon = [ "dog", "moose", "fish" ]
>>> spam == bacon
False
>>> 
>>> eggs = { "name": "Jeffrey", "species": "busta", "age": 23 }
>>> ham = { "species": "busta", "age": 23, "name": "Jeffrey" }
>>> eggs == ham
True
>>> eggs
{'name': 'Jeffrey', 'species': 'busta', 'age': 23}
>>> ham
{'species': 'busta', 'age': 23, 'name': 'Jeffrey'}
>>>
