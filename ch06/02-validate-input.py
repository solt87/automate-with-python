while True:
    age = input(  "Please enter your age: " )
    if age.isdecimal():
        break
    print( "Please enter a natural number for your age." )

while True:
    password = input( "Select  a new password (letters and numbers only): " )
    if password.isalnum():
        break
    print( "Passwords can only have letters and numbers." )