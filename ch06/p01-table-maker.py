#!/usr/bin/python3
## Take a list of lists of Strings and
## display it in a well-organized table, with righ-justified columns.
## ASSUME each inner list contains the same number of strings.


# Example data:
tableData = [['apples', 'oranges', 'cherries', 'banana'],
             ['Alice', 'Bob', 'Carol', 'David'],
             ['dogs', 'cats', 'moose', 'goose']]


def printTable( table ):
    ## Find the longest string in each column:
    greatest_len = [ 0 ] * len( table )
        
    for row in range( len ( table ) ):
        for col in range( len( table[ 0 ] ) ):
            if len( table[ row ][ col ] ) > greatest_len[ row ]:
                greatest_len[ row ] = len( table[ row ][ col ] )
    
    ## Print the table:
    for row in range( len ( table[ 0 ] ) ):
        for col in range( len( table ) ):
            print( table[ col ][ row ].rjust( greatest_len[ col ] + 1 ), end=""  )
        print()

printTable( tableData )