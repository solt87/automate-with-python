>>> "Jeffrey".isalpha()
True
>>> "Jeff rey".isalpha()
False
>>> "Jeffrey23".isalpha()
False
>>> "Jeffrey23".isalnum()
True
>>> "Jeffrey 23".isalnum()
False
>>> "23".isdecimal()
True
>>> "2 3".isdecimal()
False
>>> "2n3".isdecimal()
False
>>> "foo bar".isspace()
False
>>> " \t\n".isspace()
True
>>> "      ".isspace()
True
>>> "\t".isspace()
True
>>> "\t\t".isspace()
True
>>> "\n".isspace()
True
>>> "\n\n".isspace()
True
>>> "Jeffrey".istitle()
True
>>> "Jeffrey 23".istitle()
True
>>> " Jeffrey".istitle()
True
>>> "23Jeffrey".istitle()
True
>>> "JEffrey".istitle()
False
>>> "jEffrey".istitle()
False
>>> "jeffrey".istitle()
False
>>> "23Jeffrey".istitle()
True
>>> "JeffreY".istitle()
False
>>> "Jeffrey Jones".istitle()
True
>>> "Jeffrey jones".istitle()
False
>>> "Jeffrey Jones Smith".istitle()
True
>>> "Jeffrey Jones smith".istitle()
False
>>> "Jeffrey jones Smith".istitle()
False
>>> %Run 02-validate-input.py
Please enter your age: two
Please enter a natural number for your age.
Please enter your age: 2
Select  a new password (letters and numbers only): ****
Passwords can only have letters and numbers.
Select  a new password (letters and numbers only): mysecretpassw0rd
>>> 
