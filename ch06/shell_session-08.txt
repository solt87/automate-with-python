>>> spam = "     hello, world     "
>>> spam
'     hello, world     '
>>> spam.lstrip()
'hello, world     '
>>> spam.rstrip()
'     hello, world'
>>> spam.strip()
'hello, world'
>>> spam.rstrip().lstrip()
'hello, world'
>>> 
>>> spam = "SpamSpamBaconSpamEggsSpamSpam"
>>> spam.strip( "ampS" )
'BaconSpamEggs'
>>> 
