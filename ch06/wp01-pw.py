#!/usr/bin/python3
## A simple and *insecure* demonstrartion of a password manager.

import sys, pyperclip


PASSWORDS= {
    "email": "longandinsecureEMAILpassword",
    "blog": "longandinsecureBLOGpassword",
    "luggage": "12345"
    }

if len( sys.argv ) < 2:
    print( "Usage: python pw.py [account]" )
    sys.exit()

account = sys.argv[ 1 ]  ## First command line argument is the account name.

if account in PASSWORDS:
    pyperclip.copy( PASSWORDS[ account ] )
    print( "Password for " + account + " copied to clipboard." )
else:
    print( "There is no account named " + account + "." )

