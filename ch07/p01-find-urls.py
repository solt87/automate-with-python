#!/usr/bin/python3
## Practice: match URLs beginning with "http://" or "https://"


import re, pyperclip


#text = """Hello! Welcome to http://example.com.
#This is a real great site.
#
#Buy your shoez at https://buyshoezhere.co.biz/shoeshop.php
#
#My homepage is http:/myhomepage.domain.dom
#
#The quick brown fox jumped over the lazy dog.
#
#Duh http://www.nocollabsite.foo/index.aspx?uid=1138&netid=yo_net-rox
#
#da server iz hear: http://localhost:8080
#"""

text = pyperclip.paste()

## Set up regex object for matching
urlRegex = re.compile( r"https?://[-a-zA-Z0-9/.:\?=&_]+" )

## Match the text
foundUrls = urlRegex.findall( text )

## Sort the findings, and prepare a string version for pasting to clipboard
foundUrls.sort()

urlsString = ""

for url in foundUrls:
	urlsString += "\n" + str( url )

pyperclip.copy( urlsString )