#!/usr/bin/python3
## Find TAJ numbers in a text and replace them with X characters.


## TAJ numbers are 9-digit numbers somewhat similar in purpose to
## the Social Security Number in the U.S.
##
## This program looks for TAJ numbers of the following formats:
## - 123456789
## - 123 456 789
## - 123-456-789
##
## The TAJ numbers found are then replaxed by "XXX-XXX-XXX".


import re, pyperclip

## Setup regular expression
tajRegex = re.compile( r"""(\d{9})                |
                           (\d{3}\ \d{3}\ \d{3})  |
                           (\d{3}-\d{3}-\d{3})""" ,
                           re.VERBOSE )

## Obtain text
text = pyperclip.paste()

## Find and replace TAJ numbers
text = tajRegex.sub( "XXX-XXX-XXX", text )

## Produce the resulting text
pyperclip.copy( text )