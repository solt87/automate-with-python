#!/usr/bin/python3

## Mad Libs: read a text file, and prompt the user for a replacement
## for each occurrence of "NOUN", "VERB", "ADVERB" or "ADJECTIVE" in
## the original text, then produce the text with all the occurrences
## replaced with their corresponding user-given values.

import re

## The words that will have to be replaced
#PLACEHOLDERS = [ "ADVERB", "ADJECTIVE", "NOUN", "VERB" ]
placeholderRegex = re.compile( r"""(\W*)
                                   (ADVERB|ADJECTIVE|NOUN|VERB)
                                   (\W*)""", re.VERBOSE )

## The file to read from
FILE = "./p01-text.txt"

## Get the original text
inFile = open( FILE, "r" )
text = inFile.read().split()
#text = re.split( r"\W+", text )
inFile.close()

## Go through the text, and replace everything that needs replacing
for word in text:
    wordMo = placeholderRegex.search( word )
    if wordMo != None:
        newWord = input( "Please enter a(n) " \
                         + wordMo.group().strip().lower() + ": " )
        index = text.index( word )
        text[ index ] = wordMo.groups()[ 0 ] + newWord + wordMo.groups()[ 2 ]

## Print the result
print( " ".join( text ) )