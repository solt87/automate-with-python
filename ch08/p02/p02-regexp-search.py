#!/usr/bin/python3

## Regexp search: open all .txt files in a directory, and
## search them for a user-provided regular expression, then
## print the results.


import re, os, pprint, sys


## The directory to search for .txt files
DIR = os.getcwd()

## Get the list of files in DIR
fileList = os.listdir( DIR )

## Discard files that don't end with ".txt"
for item in fileList:
    if not item.endswith( ".txt" ):
        fileList.remove( item )

if fileList == []:
    print( "No .txt files found." )
    sys.exit()

## Ask the user for a regular expression
prompt = "Please enter a regular expression (e.g r\"foo(bar)?\"): "    
regexpIn = eval( input( prompt ) )
regexp = re.compile( regexpIn )

## Read each .txt file's contents and search for the given regexp
results = []

for file in fileList:
    ## Result: holds name of file and matches therein
    currentResult = [ file ]
    ## File handler
    fileObj = open( file, "r" )
    ## The text of the file
    text = fileObj.read()
    fileObj.close()
    ## Match the regexp on the text, store result
    mo = regexp.findall( text )
    
    if mo == []:
        currentResult.append( "No match" )
    else:
        currentResult.append( pprint.pformat( mo ) )
    
    results.append( tuple( currentResult ) )

## Print the result
print()
for item in results:
    print( item[ 0 ] + ": " + item[ 1 ] )
