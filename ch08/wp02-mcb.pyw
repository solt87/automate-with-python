#!/usr/bin/python3
## Multyclipboard: save and load multiple pieces of text to/from the clipboard.

## Usage: ./mcb.pyw save <keyword> : saves clipboard to keyword.
##        ./mcb.pyw <keyword>      : loads keyword's content to clipboard.
##        ./mcb.pyw list           : copies all keywords to the clipboard.


import shelve, pyperclip, sys


mcbShelf = shelve.open( "mcb" )

## Save clipboard content or delete keyword
if len( sys.argv ) == 3:
    if sys.argv[ 1 ].lower() == "save":
        mcbShelf[ sys.argv[ 2 ] ] = pyperclip.paste()
    elif sys.argv[ 1 ].lower() == "delete":
        del mcbShelf[ sys.argv[ 2 ] ]
                      
elif len( sys.argv ) == 2:
    ## List keywords or load content or delete all content
    if sys.argv[ 1 ].lower() == "list":
        pyperclip.copy( str( list( mcbShelf.keys() ) ) )
    elif sys.argv[ 1 ].lower() == "delete":
        for item in mcbShelf.keys():
            del mcbShelf[ item ]
    elif sys.argv[ 1 ] in mcbShelf:
        pyperclip.copy( mcbShelf[ sys.argv[ 1 ] ] )

mcbShelf.close()