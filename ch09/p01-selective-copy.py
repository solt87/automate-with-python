#!/usr/bin/python3

## Selective copy: walk through a folder tree and search for files of
## a certain extension, then copy them to a new folder.


import os, shutil


## Extension of files to copy
targetExt = "txt"

## Destination directory to copy to
destDir = "/tmp/junk"

## The directory from which to walk down
srcDir = "."

## Walk through the tree of the source dir, and copy any
## files that have to be copied

for root, dirs, files in os.walk( srcDir ):
    source = os.path.abspath( root )
    
    for file in files:
        if file.endswith( "." + targetExt ):
            ## Don't actually copy, just report what would be done
            print( "Copying %s into %s." % \
                   ( os.path.join( source, file ), destDir ) )

print( "Done." )