#!/usr/bin/python3

## Walk a directory tree and report any files that are above a
## given size.


import os


## The size limit (os.path.getsize() returns size in bytes.)
maxSize = 10000

## Directory to walk
srcDir = "../"

for rootDir, folders, files in os.walk( srcDir ):
    
    for file in files:
        
        fullFilename = os.path.join( os.path.abspath( rootDir ), file )
        fileSize = os.path.getsize( fullFilename )
        
        if fileSize > maxSize:
            print( "The file %s is greater than %s bytes: %s bytes." % \
                   ( fullFilename, str( maxSize ), str( fileSize ) ) )

print( "\nDone." )
    