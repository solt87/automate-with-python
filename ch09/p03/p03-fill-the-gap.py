#!/usr/bin/python3

## Find all files with a given prefix in the current folder, and
## check for any gaps in the numbering, then "close the gap".

## ASSUME files are named <prefix>\d\d\d.<extension>, where
## \d stands for a single digit. (E.g. spam001.txt, foo032.pdf.)


import os, re


## Constants
##############

## Filename prefix
prefix = "spam"

## Directory to searh
srcDir = "."

## Get a list of files in the current directory
haveFiles = os.listdir( srcDir )
haveFiles.sort()

#x Regular expression to get the parts of the filename
filenameRegex = re.compile( prefix + r"(\d{3})\.(\w+)" )


## Helper functions
#####################

## Get the number part from a filename
def getNum( filename ):
    return filenameRegex.match( filename ).group( 1 )

## Turn an integer into a 3-character string
def int2str( integer ):
    result = str( integer )
    
    while len( result) < 3:
        result = "0" + result
    
    return result


## Main part
##############


## Pick only the files that begin with 'prefix'
origFiles = []
for filename in haveFiles:
    
    if filename.startswith( prefix ):
        origFiles.append( filename )
    
    # print( origFiles ) ## for debugging

## We don't need the original, full file list anymore
#print( haveFiles ) ## for debugging
#print( origFiles ) ## for debugging
del( haveFiles )

## Loop over the list of filenames, and rename the files if needed
currentNum = int( getNum( origFiles[ 0 ] ) )

for index in range( 0, len( origFiles ) ):
    
    ext = filenameRegex.match( origFiles[ index ] ).group( 2 )
    destFile = prefix + int2str( currentNum ) + "." + ext
    srcFile = origFiles[ index ]
    
    #print( srcFile, destFile ) ## for debugging
    
    if srcFile != destFile:
        ## Don't actually rename, just report what would be done
        print( "%s -> %s." % ( srcFile, destFile ) )
    else:
        print( "%s" % ( srcFile ) )
    
    currentNum += 1

print( "\nDone." )