>>> import os, shutil
>>> 
>>> shutil.copy( "/etc/hosts", "/tmp/" )
'/tmp/hosts'
>>> shutil.copytree( "/home/solt/Documents/self_doc", "/dev/null" )
Traceback (most recent call last):
  File "<pyshell>", line 1, in <module>
  File "/home/solt/apps/thonny/lib/python3.6/shutil.py", line 315, in copytree
    os.makedirs(dst)
  File "/home/solt/apps/thonny/lib/python3.6/os.py", line 220, in makedirs
    mkdir(name, mode)
FileExistsError: [Errno 17] File exists: '/dev/null'
>>> shutil.copytree( "/home/solt/Documents/self_doc", "/tmp" )
Traceback (most recent call last):
  File "<pyshell>", line 1, in <module>
  File "/home/solt/apps/thonny/lib/python3.6/shutil.py", line 315, in copytree
    os.makedirs(dst)
  File "/home/solt/apps/thonny/lib/python3.6/os.py", line 220, in makedirs
    mkdir(name, mode)
FileExistsError: [Errno 17] File exists: '/tmp'
>>> shutil.copytree( "/home/solt/Documents/self_doc", "/tmp/test" )
'/tmp/test'
>>> shutil.copytree( "/home/solt/Documents/self_doc", "/dev/null/test" )
Traceback (most recent call last):
  File "<pyshell>", line 1, in <module>
  File "/home/solt/apps/thonny/lib/python3.6/shutil.py", line 315, in copytree
    os.makedirs(dst)
  File "/home/solt/apps/thonny/lib/python3.6/os.py", line 220, in makedirs
    mkdir(name, mode)
NotADirectoryError: [Errno 20] Not a directory: '/dev/null/test'
>>> shutil.move( "/tmp/hosts_copy", "/tmp/test/" )
'/tmp/test/hosts_copy'
>>> shutil.move( "/tmp/test/hosts_copy", "/tmp/hosts" )
'/tmp/hosts'
>>> shutil.copy( "/tmp/hosts", "/tmp/nonexistent/foo" )
Traceback (most recent call last):
  File "<pyshell>", line 1, in <module>
  File "/home/solt/apps/thonny/lib/python3.6/shutil.py", line 241, in copy
    copyfile(src, dst, follow_symlinks=follow_symlinks)
  File "/home/solt/apps/thonny/lib/python3.6/shutil.py", line 121, in copyfile
    with open(dst, 'wb') as fdst:
FileNotFoundError: [Errno 2] No such file or directory: '/tmp/nonexistent/foo'
>>>
