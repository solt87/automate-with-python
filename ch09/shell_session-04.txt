>>> import zipfile, os
>>> 
>>> os.chdir( "/tmp/automate_online-materials/" )
>>> os.getcwd()
'/tmp/automate_online-materials'
>>> 
>>> exampleZip = zipfile.ZipFile( "example.zip" )
>>> exampleZip.namelist()
['spam.txt', 'cats/', 'cats/catnames.txt', 'cats/zophie.jpg']
>>> spamInfo = exampleZip.getinfo( "spam.txt" )
>>> spamInfo.file_size
13908
>>> spamInfo.compress_size
3828
>>> "Compressed file is %sx smaller." % ( round( spamInfo.file_size / spamInfo.compress_size, 2 ) )
'Compressed file is 3.63x smaller.'
>>> exampleZip.close()
>>> 
>>> 
>>> 
>>> exampleZip = zipfile.ZipFile( "example.zip" )
>>> exampleZip.extractall()
>>> exampleZip.close()
>>> os.getcwd()
'/tmp/automate_online-materials'
>>> os.listdir()
['cats', 'spam.txt', 'zophie.png', 'zeroDivide.py', 'watermark.pdf', 'vampire2.py', 'vampire.py', 
'validateInput.py', 'updateProduce.py', 'updatedProduceSales.xlsx', 'twoPage.docx', 'torrentStarter.py', 
'ticTacToe.py', 'threadDemo.py', 'textMyself.py', 'swordfish.py', 'styles.xlsx', 'styled.xlsx', 'stopwatch.py', 
'sendDuesReminders.py', 'sampleChart.xlsx', 'sameName4.py', 'sameName3.py', 'sameName2.py', 'sameName.py', 
'restyled.docx', 'resizeAndAddLogo.py', 'renameDates.py', 'removeCsvHeader.zip', 'removeCsvHeader.py', 
'readDocx.py', 'readCensusExcel.py', 'randomQuizGenerator.py', 'quickWeather.py', 'pw.py', 'produceSales.xlsx', 
'printRandom.py', 'prettyCharacterCount.py', 'picnicTable.py', 'phoneAndEmail.py', 'passingReference.py', 
'myPets.py', 'multipleParagraphs.docx', 'multidownloadXkcd.py', 'mouseNow2.py', 'mouseNow.py', 'merged.xlsx', 
'meetingminutes2.pdf', 'meetingminutes.pdf', 'mcb.pyw', 'mapIt.py', 'magic8Ball2.py', 'magic8Ball.py', 
'lucky.py', 'littleKid.py', 'isPhoneNumber.py', 'inventory.py', 'helloworld.docx', 'helloFunc2.py', 
'helloFunc.py', 'hello.py', 'headings.docx', 'guests.txt', 'guessTheNumber.py', 'getDocxText.py', 
'freezeExample.xlsx', 'formFiller.py', 'fiveTimes.py', 'factorialLog.py', 'exitExample.py', 
'excelSpreadsheets.zip', 'example.zip', 'example.xlsx', 'example.html', 'example.csv', 'errorExample.py', 
'encryptedminutes.pdf', 'encrypted.pdf', 'duesRecords.xlsx', 'downloadXkcd.py', 'dimensions.xlsx', 
'dictionary.txt', 'demo.docx', 'countdown.py', 'combinePdfs.py', 'combinedminutes.pdf', 'coinFlip.py', 
'characterCount.py', 'censuspopdata.xlsx', 'census2010.py', 'catnapping.py', 'catlogo.png', 'calcProd.py', 
'bulletPointAdder.py', 'buggyAddingProgram.py', 'boxPrint.py', 'birthdays.py', 'backupToZip.py', 
'allMyCats2.py', 'allMyCats1.py', 'alarm.wav']
>>> exampleZip = zipfile.ZipFile( "example.zip" )
>>> exampleZip.extractall( "example" )
>>> exampleZip.close()
>>> 
>>> os.listdir()
['example', 'cats', 'spam.txt', 'zophie.png', 'zeroDivide.py', 'watermark.pdf', 'vampire2.py', 'vampire.py', 
'validateInput.py', 'updateProduce.py', 'updatedProduceSales.xlsx', 'twoPage.docx', 'torrentStarter.py', 
'ticTacToe.py', 'threadDemo.py', 'textMyself.py', 'swordfish.py', 'styles.xlsx', 'styled.xlsx', 'stopwatch.py', 
'sendDuesReminders.py', 'sampleChart.xlsx', 'sameName4.py', 'sameName3.py', 'sameName2.py', 'sameName.py', 
'restyled.docx', 'resizeAndAddLogo.py', 'renameDates.py', 'removeCsvHeader.zip', 'removeCsvHeader.py', 
'readDocx.py', 'readCensusExcel.py', 'randomQuizGenerator.py', 'quickWeather.py', 'pw.py', 'produceSales.xlsx', 
'printRandom.py', 'prettyCharacterCount.py', 'picnicTable.py', 'phoneAndEmail.py', 'passingReference.py', 
'myPets.py', 'multipleParagraphs.docx', 'multidownloadXkcd.py', 'mouseNow2.py', 'mouseNow.py', 'merged.xlsx', 
'meetingminutes2.pdf', 'meetingminutes.pdf', 'mcb.pyw', 'mapIt.py', 'magic8Ball2.py', 'magic8Ball.py', 
'lucky.py', 'littleKid.py', 'isPhoneNumber.py', 'inventory.py', 'helloworld.docx', 'helloFunc2.py', 
'helloFunc.py', 'hello.py', 'headings.docx', 'guests.txt', 'guessTheNumber.py', 'getDocxText.py', 
'freezeExample.xlsx', 'formFiller.py', 'fiveTimes.py', 'factorialLog.py', 'exitExample.py', 
'excelSpreadsheets.zip', 'example.zip', 'example.xlsx', 'example.html', 'example.csv', 'errorExample.py', 
'encryptedminutes.pdf', 'encrypted.pdf', 'duesRecords.xlsx', 'downloadXkcd.py', 'dimensions.xlsx', 
'dictionary.txt', 'demo.docx', 'countdown.py', 'combinePdfs.py', 'combinedminutes.pdf', 'coinFlip.py', 
'characterCount.py', 'censuspopdata.xlsx', 'census2010.py', 'catnapping.py', 'catlogo.png', 'calcProd.py', 
'bulletPointAdder.py', 'buggyAddingProgram.py', 'boxPrint.py', 'birthdays.py', 'backupToZip.py', 
'allMyCats2.py', 'allMyCats1.py', 'alarm.wav']
>>> os.listdir( "example/" )
['cats', 'spam.txt']
>>> os.listdir( "example/cats/" )
['zophie.jpg', 'catnames.txt']
>>> exampleZip = zipfile.ZipFile( "example.zip" )
>>> exampleZip.extract( "spam.txt" )
'/tmp/automate_online-materials/spam.txt'
>>> exampleZip.extract( "spam.txt", "example" )
'example/spam.txt'
>>> exampleZip.close()
>>> 
>>> 
>>> newZip = zipfile.ZipFile( "new.zip", "w" )
>>> newZip.write( "spam.txt", cpmress_type=zipfile.ZIP_DEFLATED )
Traceback (most recent call last):
  File "<pyshell>", line 1, in <module>
TypeError: write() got an unexpected keyword argument 'cpmress_type'
>>> newZip.write( "spam.txt", compress_type=zipfile.ZIP_DEFLATED )
>>> newZip.close()
>>>
