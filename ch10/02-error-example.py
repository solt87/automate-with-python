#!/usr/bin/python3

## Traceback example

def spam():
    bacon()

def bacon():
    raise Exception( "This is the error message." )

spam()