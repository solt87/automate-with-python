#!/usr/bin/python3

## Example program for a debug session

print( "Enter the first number to add:" )
first = input()
print( "Enter the second number to add:" )
second = input()
print( "Enter the third number to add:" )
third = input()

print( "The sum is " + first + second + third + "." )
