#!/usr/bin/python3

## Coin-toss program for debug demonstration


import random


heads = 0

for i in range( 1, 1001 ):
    if random.randint( 0, 1 ) == 1:
        heads = heads + 1
    if i == 500:
        print( "Halfway done." )  ## Set breakpoint here for demo.

print( "Heads came up " + str( heads ) + " times." )
