>>> %Run pp01-coin-toss-solution.py
Guess the coin toss! Enter heads or tails:
heads
Nope! Guess again!
tails
You got it!
>>> %Run pp01-coin-toss-solution.py
Guess the coin toss! Enter heads or tails:
tails
You got it!
>>> %Run pp01-coin-toss-solution.py
Guess the coin toss! Enter heads or tails:
gooby
Guess the coin toss! Enter heads or tails:
dolan
Guess the coin toss! Enter heads or tails:
tails
Nope! Guess again!
heads
You got it!
>>> %Run pp01-coin-toss-solution.py
Guess the coin toss! Enter heads or tails:
heads
Nope! Guess again!
heads
Nope. You are really bad at this game.
>>> %Run pp01-coin-toss-solution.py
Guess the coin toss! Enter heads or tails:
tails
Nope! Guess again!
tails
Nope. You are really bad at this game.
>>>
