#!/usr/bin/python3
## Starter file for practice project (debugging)


import random


guess = ""

coinSides = ( "heads", "tails" )

while guess not in coinSides:
    print( "Guess the coin toss! Enter heads or tails:" )
    guess = input()
    
toss = random.choice( coinSides )

if toss == guess:
    print( "You got it!" )
else:
    print( "Nope! Guess again!" )
    guess = input()
    if toss == guess:
       print( "You got it!" )
    else:
        print( "Nope. You are really bad at this game." )