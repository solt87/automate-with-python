#!/usr/bin/python3

eggs = "yO"
bacon = "Yo"

assert eggs.lower() != bacon.lower(), \
       "'eggs' and 'bacon' should not contain equivalent strings!"
