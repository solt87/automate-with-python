#!/usr/bin/env python3
## Download a set of given BKK timetables

## TODO:
## - add meaningful logging for e.g. downloading files, checking for
##   directory entries.


import hashlib, logging, os, requests, shutil

logging.basicConfig( filename = "/tmp/timetable_test.log",
                     format = "%(asctime)s\t%(levelname)s\t%(message)s",
                     level = logging.DEBUG )

logging.info( "BEGIN PROGRAM" )


##############
## Constants

## Temporary directory to hold the downloaded files
tmpDir = "/tmp/timetables"
logging.debug( "tmpDir is set to %s." % ( tmpDir ) )

## Directory holding the timetables
timeTableDir = os.path.expanduser( "~/Documents/menetrendek" )
logging.debug( "timeTableDir is set to %s." % ( timeTableDir ) )

## The wanted timetables
timeTables = {
    "baseUrl": "https://bkk.hu/apps/menetrend/pdf/",
    "33": {
        "be": "0330/20180630/42.pdf"
        },
    "58": {
        "le": "0580/20171112/40.pdf",
        "fel": "0580/20171112/35.pdf"
        },
    "101B": {
        "le": "1012/20161027/5.pdf",
        "fel": "1012/20161027/20.pdf"
        },
    "101E": {
        "le": "1015/20161027/5.pdf",
        "fel": "1015/20161027/21.pdf"
        },
    "133E": {
        "be": "1335/20170102/50.pdf"
        },
    "141": {
        "le": "1410/20170201/26.pdf",
        "fel": "1410/20170201/14.pdf"
        }
    }


##############
## Functions


## Return the digest of the given file
def get_hash( fileName ):
    logging.debug( "Begin function get_hash" )
    hashObj = hashlib.sha256()
    
    with open( fileName, mode = "rb" ) as fd:
        hashObj.update( fd.read() )
        fd.close()
    
    logging.debug( "End function get_hash" )
    return hashObj.digest()

## Return True if the hashes of the given two files are the same
def hashes_match( file1, file2 ):
    logging.debug( "Called function hashes_match" )
    return get_hash( file1 ) == get_hash( file2 )


##############
## Main part


## Download each timeatable file to the temporary directory
if os.path.isdir( tmpDir ):
    logging.debug( "Found existing tmpDir %s" % ( tmpDir ) )
    shutil.rmtree( tmpDir )
    logging.debug( "Removed existing tmpDir %s" % ( tmpDir ) )
else:
    logging.debug( "No existing tmpDir %s found" % ( tmpDir ) )

logging.debug( "Creating tmpDir %s" % ( tmpDir ) )
os.mkdir( tmpDir )
logging.debug( "Entering tmpDir %s" % ( tmpDir ) )
os.chdir( tmpDir )

logging.info( "Begin downloading files" )

for busNo in timeTables.keys():
    logging.debug( "busNo is set to %s" % ( busNo ) )
    if busNo == "baseUrl":
        continue
    
    for direction in timeTables[ busNo ].keys():
        fileName = busNo.lower() + "_" + direction + ".pdf"
        logging.debug( "Filename is set to %s" % ( fileName ) )
        
        url = timeTables[ "baseUrl" ] + timeTables[ busNo ][ direction ]
        logging.debug( "url is set to %s" % ( url ) )
        res = requests.get( url )
        res.raise_for_status()
        
        with open( fileName, "wb" ) as fileDescriptor:
            logging.debug( "Opened file %s for writing" % ( fileName ) )
            for chunk in res.iter_content( chunk_size=100000 ):
                fileDescriptor.write( chunk )
            fileDescriptor.close()
            logging.debug( "Closed file %s"  % ( fileName ) )

logging.info( "Finished downloading files" )

## Compare the hashes of the downloaded files to that of their
## corresponding, already stored counterparts
logging.info( "Begin copying files" )

if not os.path.isdir( timeTableDir ):
    logging.debug( "No pre-existing timeTableDir %s found, creating it" % \
                   ( timeTableDir ) )
    os.mkdir( timeTableDir )
else:
    logging.debug( "Found timeTableDir %s" % ( timeTableDir ) )

for newFile in os.listdir( tmpDir ):
    oldFile = os.path.join( timeTableDir, os.path.basename( newFile ) )
    
    ## If there is no older file, or the hashes of the old and new files don't
    ## match, copy the new file to the destination directory:
    if not ( os.path.isfile( oldFile ) and hashes_match( oldFile, newFile ) ):
        logging.info( "Copying %s to %s" % ( oldFile, newFile ) )
        shutil.copy2( newFile, oldFile )
    else:
        logging.info( "Target file %s exists and is up to date" % ( newFile ) )

logging.info( "Finished copying files" )
## Remove the temporary directory and its contents
logging.debug( "Removing tmpDir %s\n" % ( tmpDir ) )
shutil.rmtree( tmpDir )

logging.info( "END PROGRAM\n" )
