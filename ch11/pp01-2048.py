#!/usr/bin/python3
## Play the 2048 game by repeatedly "pressing" up, right, left, right.


from selenium import webdriver
from selenium.webdriver.common.keys import Keys


GAMESITE = "https://gabrielecirulli.github.io/2048/"


## Go to the game page
browser = webdriver.Firefox()
browser.get( GAMESITE )


## Keep sending up, down, left, right keys
htmlElem = browser.find_element_by_tag_name( "html" )

while True:
    for key in [ Keys.UP, Keys.RIGHT, Keys.DOWN, Keys.LEFT ]:
        htmlElem.send_keys( key )