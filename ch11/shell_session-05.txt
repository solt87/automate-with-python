Python 3.6.4
>>> import requests, bs4
/home/solt/.thonny/BundledPython36/lib/python3.6/site-packages/requests/__init__.py:80: 
RequestsDependencyWarning: urllib3 (1.23) or chardet (3.0.4) doesn't match a supported version!
  RequestsDependencyWarning)
>>> res = requests.get( "https://nostarch.com" )
>>> res.raise_for_status()
>>> noStarchSoup = bs4.BeautifulSoup( res.text )
/home/solt/.thonny/BundledPython36/lib/python3.6/site-packages/bs4/__init__.py:181: UserWarning: No parser was 
explicitly specified, so I'm using the best available HTML parser for this system ("lxml"). This usually isn't a 
problem, but if you run this code on another system, or in a different virtual environment, it may use a 
different parser and behave differently.

The code that caused this warning is on line 41 of the file 
/home/solt/apps/thonny/lib/python3.6/site-packages/thonny/shared/backend_launcher.py. To get rid of this 
warning, change code that looks like this:

 BeautifulSoup(YOUR_MARKUP})

to this:

 BeautifulSoup(YOUR_MARKUP, "lxml")

  markup_type=markup_type))
>>> type( noStarchSoup )
<class 'bs4.BeautifulSoup'>
>>> 
>>> exampleFile = open( "example.html", "r" )
>>> exampleSoup = bs4.BeautifulSoup( exampleFile )
>>> type( exampleSoup )
<class 'bs4.BeautifulSoup'>
>>> 
>>> elems = exampleSoup.select( "#author" )
>>> type( elems )
<class 'list'>
>>> len( elems )
1
>>> type( elems[ 0 ] )
<class 'bs4.element.Tag'>
>>> elems[f 0 ].getText()
  File "<pyshell>", line 1
    elems[f 0 ].getText()
            ^
SyntaxError: invalid syntax

>>> elems[ 0 ].getText()
'OG Loc'
>>> type( elems[ 0 ].getText() )
<class 'str'>
>>> str( elems[ 0 ].getText() )
'OG Loc'
>>> str( elems[ 0 ].attrs )
"{'id': 'author'}"
>>> elems[ 0 ].attrs
{'id': 'author'}
>>> str( elems[ 0 ] )
'<span id="author">OG Loc</span>'
>>> 
>>> pElems = exampleSoup.select( "p" )
>>> str( pElems )
'[<p>Download my <strong>Gangsta</strong> book from <a href="http://\nexample.com">my website</a>.</p>, <p 
class="slogan">Keepin\' it real, str8 from da streetz!</p>, <p>By <span id="author">OG Loc</span></p>]'
>>> type( pElems )
<class 'list'>
>>> pElems[ 0 ].getText()
'Download my Gangsta book from my website.'
>>> str( pElems[ 1 ] )
'<p class="slogan">Keepin\' it real, str8 from da streetz!</p>'
>>> pElems[ 1 ].getText()
"Keepin' it real, str8 from da streetz!"
>>> str( pElems[ 2 ] )
'<p>By <span id="author">OG Loc</span></p>'
>>> pElems[ 2 ].getText()
'By OG Loc'
>>> 
>>> spanElem = exampleSoup.select( "span" )[ 0 ]
>>> str( spanElem )
'<span id="author">OG Loc</span>'
>>> spanElem.get( "id" )
'author'
>>> spanElem.get( "some_nonexiztent_thang" ) == None
True
>>> spanElem.attrs
{'id': 'author'}
>>> 
