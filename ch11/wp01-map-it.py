#!/usr/bin/python3
## Read an address form the command line or from the clipboard and
## open a web browser to the Google Maps page for that address.


import webbrowser, sys, pyperclip


if len( sys.argv ) > 1:
    ## Get address from command line
    address = " ".join( sys.argv[ 1: ] ) ## Multiple unquoted args? WTF?
else:
    ## Get address from clipboard
    address = pyperclip.paste()

webbrowser.open( "https://www.google.com/maps/place/" + address )
