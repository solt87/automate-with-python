#!/usr/bin/python3
## lucky.py: Opens several Google search results


import requests, sys, webbrowser, bs4


print( "Googling..." )  ## Display text while downloading the search page

res = requests.get( "https://google.com/search?q=" + " ".join( sys.argv[ 1: ] ) )
res.raise_for_status()


## Retrieve top search result links
soup = bs4.BeautifulSoup( res.text )

## Open a browser tab for each result
linkElements = soup.select( ".r a" )

numOpen = min( 5, len( linkElements ) )
for i in range( numOpen ):
    webbrowser.open( "https://google.com/" + linkElements[ i ].get( "href" ) )

