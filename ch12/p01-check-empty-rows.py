#! /usr/bin/env python3
## Check for and report any empty rows in an Excel file.


import openpyxl
from openpyxl.utils import get_column_letter, column_index_from_string


wb = openpyxl.load_workbook( "has-empty.xlsx" )
sheet = wb.active

## Go over each row, then check if there's at least one non-empty cell in
## that row
for row in range( 1, sheet.max_row + 1 ):
    rowStr = str( row )
    onlyEmpty = True
    
    for col in range( 1, sheet.max_column + 1 ):
         colLet = get_column_letter( col )
         #print( sheet[ colLet + rowStr ].value )
         if sheet[ colLet + rowStr ].value != None:
             onlyEmpty = False
             break
    
    if onlyEmpty:
        print( "Row %s is an empty row." % ( rowStr ) )
