#˘/usr/bin/env python3
## Make an n*n multiplication table xlsx, where n is given as an argument


import openpyxl, sys
from openpyxl.utils import get_column_letter, column_index_from_string
from openpyxl.styles import Font


if len( sys.argv ) != 2:
    raise TypeError( "program expects 1 integer argument" )

daNum = int( sys.argv[ 1 ] )

## Create workbook and worksheet
wb = openpyxl.Workbook()
sheet = wb.active

## Create label row and column
bold = Font( name = "Times New Roman", bold = True )
for colNum in range( 2, daNum + 2 ):  ## +1 for range-end, +1 for label column
    col = get_column_letter( colNum )
    sheet[ col + "1" ].font = bold
    sheet[ col + "1" ] = colNum - 1

for row in range( 2, daNum + 2 ):
    sheet[ "A" + str( row ) ].font = bold
    sheet[ "A" + str( row ) ] = row - 1        

## Create the actual multiplication table
for row in range( 2, daNum + 2 ):  ## +1 for range-end, +1 for label row
    for colNum in range( 2, daNum + 2 ):
        col = get_column_letter( colNum )
        sheet[ col + str( row ) ] = ( row -1 ) * ( colNum - 1 )

## Save the file
wb.save( "multiplication_table_" + str( daNum ) + ".xlsx" )