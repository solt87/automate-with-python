>>> sheet
<Worksheet "Sheet1">
>>> tuple( sheet"A1":"C3" )
  File "<pyshell>", line 1
    tuple( sheet"A1":"C3" )
                   ^
SyntaxError: invalid syntax

>>> tuple( sheet[ "A1":"C3" ] )
((<Cell 'Sheet1'.A1>, <Cell 'Sheet1'.B1>, <Cell 'Sheet1'.C1>), (<Cell 'Sheet1'.A2>, <Cell 'Sheet1'.B2>, <Cell 
'Sheet1'.C2>), (<Cell 'Sheet1'.A3>, <Cell 'Sheet1'.B3>, <Cell 'Sheet1'.C3>))
>>> for rowOfCellObjects in sheet[ "A1":"C3" ]:
    for cellObj in rowOfCellObjects:
        print( cellObj.coordinate, cellObj.value )
    print( "--- END OF ROW ---" )

A1 2015-04-05 13:34:02
B1 Apples
C1 73
--- END OF ROW ---
A2 2015-04-05 03:41:23
B2 Cherries
C2 85
--- END OF ROW ---
A3 2015-04-06 12:46:51
B3 Pears
C3 14
--- END OF ROW ---
>>> 
>>> sheet = wb.active
>>> sheet
<Worksheet "Sheet1">
>>> /home/solt/.thonny/jedi_0.10.2/jedi/evaluate/compiled/__init__.py:328: DeprecationWarning: Call to 
deprecated function merged_cell_ranges (Use ws.merged_cells.ranges).
  getattr(obj, name)
sheet.columns[ 1 ]
Traceback (most recent call last):
  File "<pyshell>", line 1, in <module>
TypeError: 'generator' object is not subscriptable
>>> sheet.columns[ 1 ]
Traceback (most recent call last):
  File "<pyshell>", line 1, in <module>
TypeError: 'generator' object is not subscriptable
>>> sheet.columns
<generator object Worksheet._cells_by_col at 0x7f776568c200>
>>> /home/solt/.thonny/jedi_0.10.2/jedi/evaluate/compiled/__init__.py:328: DeprecationWarning: Call to 
deprecated function merged_cell_ranges (Use ws.merged_cells.ranges).
  getattr(obj, name)
sheet.get_columns()
Traceback (most recent call last):
  File "<pyshell>", line 1, in <module>
AttributeError: 'Worksheet' object has no attribute 'get_columns'
>>> /home/solt/.thonny/jedi_0.10.2/jedi/evaluate/compiled/__init__.py:328: DeprecationWarning: Call to 
deprecated function merged_cell_ranges (Use ws.merged_cells.ranges).
  getattr(obj, name)
/home/solt/.thonny/jedi_0.10.2/jedi/evaluate/compiled/__init__.py:509: DeprecationWarning: Call to deprecated 
function merged_cell_ranges (Use ws.merged_cells.ranges).
  obj = getattr(obj, name)

>>> sheet.columns()
Traceback (most recent call last):
  File "<pyshell>", line 1, in <module>
TypeError: 'generator' object is not callable
>>> tuple( sheet.columns )
((<Cell 'Sheet1'.A1>, <Cell 'Sheet1'.A2>, <Cell 'Sheet1'.A3>, <Cell 'Sheet1'.A4>, <Cell 'Sheet1'.A5>, <Cell 
'Sheet1'.A6>, <Cell 'Sheet1'.A7>), (<Cell 'Sheet1'.B1>, <Cell 'Sheet1'.B2>, <Cell 'Sheet1'.B3>, <Cell 
'Sheet1'.B4>, <Cell 'Sheet1'.B5>, <Cell 'Sheet1'.B6>, <Cell 'Sheet1'.B7>), (<Cell 'Sheet1'.C1>, <Cell 
'Sheet1'.C2>, <Cell 'Sheet1'.C3>, <Cell 'Sheet1'.C4>, <Cell 'Sheet1'.C5>, <Cell 'Sheet1'.C6>, <Cell 
'Sheet1'.C7>))
>>> tuple( sheet.columns )[ 1 ]
(<Cell 'Sheet1'.B1>, <Cell 'Sheet1'.B2>, <Cell 'Sheet1'.B3>, <Cell 'Sheet1'.B4>, <Cell 'Sheet1'.B5>, <Cell 
'Sheet1'.B6>, <Cell 'Sheet1'.B7>)
>>> for cellObj in tuple( sheet.columns )[ 1 ]:
    print( cellObj.value )

Apples
Cherries
Pears
Oranges
Apples
Bananas
Strawberries
>>> for cellObj in tuple( sheet.rows )[ 1 ]:
    print( cellObj.value )

2015-04-05 03:41:23
Cherries
85
>>> for cellObj in tuple( sheet.rows )[ 0 ]:
    print( cellObj.value )

2015-04-05 13:34:02
Apples
73
>>> 
