#! /usr/bin/env python3


import docx


def getText( fileName ):
    doc = docx.Document( fileName )
    fullText = []
    
    for para in doc.paragraphs:
        fullText.append( para.text )
    
    return "\n".join( fullText )
