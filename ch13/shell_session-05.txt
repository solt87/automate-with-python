>>> import docx
>>> 
>>> doc = docx.Document( "../../automate_online-materials/demo.docx" )
>>> doc.paragraphs[ 0 ].text
'Document Title'
>>> doc.paragraphs[ 0 ].style
_ParagraphStyle('Title') id: 140700884257368
>>> doc.paragraphs[ 0 ].style = "Normal"
>>> doc.paragraphs[ 0 ].style
_ParagraphStyle('Normal') id: 140700884257424
>>> doc.paragraphs[ 1 ].text
'A plain paragraph with some bold and some italic'
>>> doc.paragraphs[ 1 ].runs[ 0 ].text, doc.paragraphs[ 1 ].runs[ 1 ].text, doc.paragraphs[ 1 ].runs[ 2 ].text, 
doc.paragraphs[ 1 ].runs[ 3 ].text
('A plain paragraph with', ' some ', 'bold', ' and some ')
>>> doc.paragraphs[ 1 ].runs[ 0 ].text, doc.paragraphs[ 1 ].runs[ 1 ].text, doc.paragraphs[ 1 ].runs[ 2 ].text, 
doc.paragraphs[ 1 ].runs[ 3 ].text, doc.paragraphs[ 1 ].runs[ 4 ].text
('A plain paragraph with', ' some ', 'bold', ' and some ', 'italic')
>>> 
>>> doc.paragraphs[ 1 ].runs[ 0 ].style = "QuoteChar"
/home/solt/.thonny/BundledPython36/lib/python3.6/site-packages/docx/styles/styles.py:143: UserWarning: style 
lookup by style_id is deprecated. Use style name as key instead.
  return self._get_style_id_from_style(self[style_name], style_type)
>>> doc.paragraphs[ 1 ].runs[ 0 ].style
<docx.styles.style._CharacterStyle object at 0x7ff77a328e48>
>>> doc.paragraphs[ 1 ].runs[ 0 ].style = "Quote Char"
>>> doc.paragraphs[ 1 ].runs[ 0 ].style
<docx.styles.style._CharacterStyle object at 0x7ff77a328320>
>>> doc.paragraphs[ 1 ].runs[ 1 ].underline = True
>>> doc.paragraphs[ 1 ].runs[ 3 ].underline = True
>>> doc.save( "../../automate_online-materials/restyled.docx" )
>>> 
