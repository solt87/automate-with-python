>>> import docx
>>> 
>>> doc = docx.Document()
>>> doc.add_paragraph( "hello, world" )
<docx.text.paragraph.Paragraph object at 0x7ff77a39a898>
>>> doc.save( "../../automate_online-materials/helloworld.docx" )
>>> 
>>> 
>>> del doc
>>> doc = docx.Document()
>>> doc.add_paragraph( "hello, world" )
<docx.text.paragraph.Paragraph object at 0x7ff77a342fd0>
>>> paraObj1 = doc.add_paragraph( "Yo, this a second paragraph." )
>>> paraObj2 = doc.add_paragraph( "And this, foo', is anotha' paragraph." )
>>> paraObj1.add_run( "This is added to the second paragraph as an afterthought, dude." )
<docx.text.run.Run object at 0x7ff77a393ba8>
>>> doc.save( "../../automat_online-materials/multiparagraph.docx" )
Traceback (most recent call last):
  File "<pyshell>", line 1, in <module>
  File "/home/solt/.thonny/BundledPython36/lib/python3.6/site-packages/docx/document.py", line 142, in save
    self._part.save(path_or_stream)
  File "/home/solt/.thonny/BundledPython36/lib/python3.6/site-packages/docx/parts/document.py", line 130, in 
save
    self.package.save(path_or_stream)
  File "/home/solt/.thonny/BundledPython36/lib/python3.6/site-packages/docx/opc/package.py", line 160, in save
    PackageWriter.write(pkg_file, self.rels, self.parts)
  File "/home/solt/.thonny/BundledPython36/lib/python3.6/site-packages/docx/opc/pkgwriter.py", line 32, in write
    phys_writer = PhysPkgWriter(pkg_file)
  File "/home/solt/.thonny/BundledPython36/lib/python3.6/site-packages/docx/opc/phys_pkg.py", line 141, in 
__init__
    self._zipf = ZipFile(pkg_file, 'w', compression=ZIP_DEFLATED)
  File "/home/solt/apps/thonny/lib/python3.6/zipfile.py", line 1090, in __init__
    self.fp = io.open(file, filemode)
FileNotFoundError: [Errno 2] No such file or directory: '../../automat_online-materials/multiparagraph.docx'
>>> doc.save( "../../automate_online-materials/multiparagraph.docx" )
>>> 
