#! /usr/bin/env python3
## Combines all PDFs in the current directory into
## a single PDF.


import os, PyPDF2


## Get the names of all the PDF files:
pdfFiles = []

for fileName in os.listdir():
    if fileName.endswith( ".pdf" ):
        pdfFiles.append( fileName )

pdfFiles.sort( key = str.lower )


## Combine the PDFs
pdfWriter = PyPDF2.PdfFileWriter()

## Loop thourgh all the PDF files
for fileName in pdfFiles:
    pdfFileObj = open( fileName, "rb" )
    pdfReader = PyPDF2.PdfFileReader( pdfFileObj )

    ## Loop through all the pages (except the first) of the file ad add them.
    for pageNum in range( 1, pdfReader.numPages ):
        pageObj = pdfReader.getPage( pageNum )
        pdfWriter.addPage( pageObj )
    
    pdfFileObj.close()

## Save the resulting PDF to a file.
pdfOutputFile = open( "allminutes.pdf", "wb" )
pdfWriter.write( pdfOutputFile )
pdfOutputFile.close()