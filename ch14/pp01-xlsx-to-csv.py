#! /usr/bin/env python3
## Convert xlsx file worksheets to csv files.


import csv, openpyxl, os
from openpyxl.utils import get_column_letter, column_index_from_string

## Loop through all the files in the current directory
for fileName in os.listdir():
    if not fileName.endswith( ".xlsx"):
        continue
    
    ## Open one xlsx file at a time
    workbook = openpyxl.load_workbook( fileName )
    
    ## Loop through all sheets
    for sheetName in workbook.sheetnames:
        sheet = workbook[ sheetName ]
        
        ## Create CSV filename and the writer object
        csvFileName = fileName[ :fileName.find( ".xlsx" ) ] + \
                      "_" + sheetName + ".csv"
        csvFileObj = open( csvFileName, "w", newline = "" )
        csvWriter = csv.writer( csvFileObj, dialect = "unix" )

        ## Loop through each row in the xlsx file
        for rowNum in range( 1, sheet.max_row + 1 ):
            rowData = []
            
            ## Loop through each cell in the row
            for colNum in range( 1, sheet.max_column + 1 ):
                colValue = sheet[ get_column_letter( colNum ) + str( rowNum ) ].value
                rowData.append( colValue )

            ## Write the row to the CSV writer
            csvWriter.writerow( rowData )
        
        ## Close the csv file
        csvFileObj.close()
