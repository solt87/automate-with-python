#! /usr/bin/env python3
## Remove the header from all CSV files in the current directory.


import csv, os


os.makedirs( "header_removed", exist_ok=True )


## Loop through every file in the current directory
for csvFileName in os.listdir( "." ):
    if not csvFileName.endswith( ".csv" ):
        continue
    
    print( "Removing header from " + csvFileName + "...." )
    
    ## Read the CSV file, skipping the first row (=header)
    csvRows = []
    csvFileObj = open( csvFileName, "r" )
    readerObj = csv.reader( csvFileObj )
    
    for row in readerObj:
        if readerObj.line_num == 1:
            continue
        
        csvRows.append( row )
    
    csvFileObj.close()
    
    ## Write the new, headerless CSV file
    csvFileObj = open( os.path.join( "header_removed", csvFileName ), "w",
                       newline="" )
    csvWriter = csv.writer( csvFileObj )
    
    for row in csvRows:
        csvWriter.writerow( row )
    
    csvFileObj.close()
