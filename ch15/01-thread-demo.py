#! /usr/bin/env python3
## A simple threading demo


import threading, time


print( "Start of program." )

def takeANap():
    time.sleep( 5 )
    print( "Wake up!" )
    return

threadObj = threading.Thread( target = takeANap )
threadObj.start()

print( "End of program." )
