#! /usr/bin/env python3
## A simple countdown script.


import subprocess, time


timeLeft = 5  ## Change this value according to your needs =)

while timeLeft > 0:
    print( timeLeft, end=" " )
    time.sleep( 1 )
    timeLeft -= 1

## At the end of the countdown, play a sound file.
subprocess.Popen( [ "xdg-open", "alarm.wav" ] )
