>>> import imapclient
>>> 
>>> imapObj = imapclient.IMAPClient( "posteo.de", ssl = True )
>>> imapObj
<imapclient.imapclient.IMAPClient object at 0x7f0b7195d3c8>
>>> type( imapObj )
<class 'imapclient.imapclient.IMAPClient'>
>>> imapObj.login( "<REDACTED>@posteo.de", "myfakepassword" )
Traceback (most recent call last):
  File "/home/solt/.thonny/BundledPython36/lib/python3.6/site-packages/imapclient/imapclient.py", line 342, in 
login
    unpack=True,
  File "/home/solt/.thonny/BundledPython36/lib/python3.6/site-packages/imapclient/imapclient.py", line 1538, in 
_command_and_check
    typ, data = meth(*args)
  File "/home/solt/apps/thonny/lib/python3.6/imaplib.py", line 593, in login
    raise self.error(dat[-1])
imaplib.IMAP4.error: b'[AUTHENTICATIONFAILED] Authentication failed.'

During handling of the above exception, another exception occurred:

Traceback (most recent call last):
  File "<pyshell>", line 1, in <module>
  File "/home/solt/.thonny/BundledPython36/lib/python3.6/site-packages/imapclient/imapclient.py", line 345, in 
login
    raise exceptions.LoginError(str(e))
imapclient.exceptions.LoginError: b'[AUTHENTICATIONFAILED] Authentication failed.'
>>> 

