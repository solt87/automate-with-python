>>> import imapclient
>>> 
>>> imapObj = imapclient.IMAPClient( "posteo.de", ssl = True )
>>> 
>>> import getpass
>>> 
>>> imapObj.login( getpass.getpass("Address: "), getpass.getpass() )
Address: 
Password: 
'Logged in'
>>> 
>>> import pprint
>>> 
>>> pprint.pprint( imapObj.list_folders() )
[(('\\HasNoChildren', '\\Junk'), '.', u'Junk'),
 (('\\HasNoChildren', '\\Drafts'), '.', u'Drafts'),
 (('\\HasNoChildren',), '.', u'Posteo'),
 (('\\HasNoChildren', '\\Trash'), '.', u'Trash'),
 (('\\HasNoChildren',), '.', u'Queue'),
-- REDACTED --
 (('\\HasNoChildren', '\\Sent'), '.', u'Sent'),
 (('\\HasNoChildren',), '.', u'Notes'),
 (('\\HasNoChildren',), '.', u'INBOX')]
>>> imapObj.select_foler( "Filer.Accounts.Gitlab", readonly = True )
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
AttributeError: 'IMAPClient' object has no attribute 'select_foler'
>>> imapObj.select_folder( "Filer.Accounts.Gitlab", readonly = True )
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
  File "/tmp/pyjunk/lib/python2.7/site-packages/imapclient/imapclient.py", line 681, in select_folder
    self._command_and_check('select', self._normalise_folder(folder), readonly)
  File "/tmp/pyjunk/lib/python2.7/site-packages/imapclient/imapclient.py", line 1538, in _command_and_check
    typ, data = meth(*args)
  File "/usr/lib64/python2.7/imaplib.py", line 662, in select
    typ, dat = self._simple_command(name, mailbox)
  File "/usr/lib64/python2.7/imaplib.py", line 1083, in _simple_command
    return self._command_complete(name, self._command(name, *args))
  File "/usr/lib64/python2.7/imaplib.py", line 872, in _command
    raise self.abort('socket error: %s' % val)
imaplib.abort: socket error: [Errno 104] Connection reset by peer
>>> imapObj.login( getpass.getpass("Address: "), getpass.getpass() )
Address: 
Password: 
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
  File "/tmp/pyjunk/lib/python2.7/site-packages/imapclient/imapclient.py", line 345, in login
    raise exceptions.LoginError(str(e))
imapclient.exceptions.LoginError: command LOGIN illegal in state AUTH, only allowed in states NONAUTH
>>> imapObj.logout()
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
  File "/tmp/pyjunk/lib/python2.7/site-packages/imapclient/imapclient.py", line 380, in logout
    self._check_resp('BYE', 'logout', typ, data)
  File "/tmp/pyjunk/lib/python2.7/site-packages/imapclient/imapclient.py", line 1429, in _check_resp
    raise exceptions.IMAPClientError("%s failed: %s" % (command, to_unicode(data[0])))
imaplib.error: logout failed: <class 'imaplib.abort'>: socket error: [Errno 32] Broken pipe
>>> 

-- CONNECTION RE_ESTABLISHED --

>>> imapObj.select_folder( "Filer.Accounts.GitLab", readonly = True )
{'EXISTS': 7, 'PERMANENTFLAGS': (), 'HIGHESTMODSEQ': 5, 'UIDNEXT': 8, 'FLAGS': ('\\Answered', '\\Flagged', 
'\\Deleted', '\\Seen', '\\Draft'), 'UIDVALIDITY': 1469909821, 'READ-ONLY': [''], 'RECENT': 0}
>>> UIDs = imapObj.search( "ALL" )
>>> UIDs
[1, 2, 3, 4, 5, 6, 7]
>>> rawMessages = imapObj.fetch( 1, [ "BODY[]" ] )
>>> len( rawMessages )
7
>>> import pyzmail
>>> 
>>> message = pyzmail.PyzMessage.factory( rawMessages[ 1 ][ "BODY[]" ] )
>>> type( message )
<type 'instance'>
>>> message.get_subject()
u'Confirmation instructions'
>>> message.get_Addresses( "from" )
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
AttributeError: PyzMessage instance has no attribute 'get_Addresses'
>>> message.get_addresses( "from" )
[(u'GitLab', 'gitlab@mg.gitlab.com')]
>>> message.get_addresses( "to" )
[(u'<REDACTED>', '<REDACTED>')]
>>> message.get_addresses( "cc" )
[]
>>> message.get_addresses( "bcc" )
[]
>>> message.text_part == None
False
>>> message.html_part == None
False
>>> message.text_part.get_payload().decode( message.text_part.charset )
u'Welcome, Solt Budav\xe1ri!\r\n\r\nYou can confirm your email (<REDACTED>) through the link 
below:\r\n\r\nhttps://gitlab.com/users/confirmation?confirmation_token=<REDACTED>\r\n'
>>> print( message.text_part.get_payload().decode( message.text_part.charset ) )
Welcome, Solt Budavári!

You can confirm your email (<REDACTED>) through the link below:

https://gitlab.com/users/confirmation?confirmation_token=<REDACTED>

>>> print( message.html_part.get_payload().decode( message.html_part.charset )  )
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" 
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
<meta content="text/html; charset=UTF-8" http-equiv="Content-Type" />

-- LOTS OF LINES HERE --

</tbody>
</table>
</body>
</html>

>>> imapObj.logout()
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
  File "/tmp/pyjunk/lib/python2.7/site-packages/imapclient/imapclient.py", line 380, in logout
    self._check_resp('BYE', 'logout', typ, data)
  File "/tmp/pyjunk/lib/python2.7/site-packages/imapclient/imapclient.py", line 1429, in _check_resp
    raise exceptions.IMAPClientError("%s failed: %s" % (command, to_unicode(data[0])))
imaplib.error: logout failed: <class 'imaplib.abort'>: socket error: [Errno 104] Connection reset by peer
>>>
