#! /usr/bin/python3
## Resize all image in the current directory to
## fit a 300x300 pixel square, and add a logo to the lower
## right corner.


import os
from PIL import Image


SQUARE_FIT_SIZE = 300
LOGO_FILENAME = "putpixel.png"
TARGET_DIR = "/tmp/with-logo"

logoIm = Image.open( LOGO_FILENAME )
logoWidth, logoheight = logoIm.size

os.makedirs( TARGET_DIR, exist_ok = True )

## Loop over all files in the working directory
for filename in os.listdir( "." ):
    if not ( filename.endswith( ".png" ) or filename.endswith( ".jpg" ) ) \
       or filename == LOGO_FILENAME:
        continue  ## Skip non-image files and the logo itself
    
    im = Image.open( filename )
    print( filename )
    width, height = im.size

    ## Check if image needs to be resized
    if width > SQUARE_FIT_SIZE and height > SQUARE_FIT_SIZE:
        ## Calculate the new width and height to resize to
        if width > height:
            height = int( ( SQUARE_FIT_SIZE / width ) * height )
            width = SQUARE_FIT_SIZE
        else:
            width = int( ( SQUARE_FIT_SIZE / height ) * width )
            height = SQUARE_FIT_SIZE
    
    ## Resize the image
    print( "    Resizing...." )
    im = im.resize( ( width, height ) )

    ## Add the logo
    print( "    Adding logo....\n" )
    im.paste( logoIm, ( width - logoWidth, height - logoheight ), logoIm )
    
    ## Save changes
    im.save( os.path.join( TARGET_DIR, filename ) )
