#! /usr/bin/python3
## Automatically fill the form: http://autbor.com/form


import pyautogui, time


pyautogui.PAUSE = 0.5

## Adjust these to your specific setup
nameField = ( 670, 250 )
submitButton = ( 657, 777 )
submitButtonColor = ( 75, 141, 249 )
submitAnotherLink = ( 750, 160 )

## Test form data
formData = [{'name': 'Alice', 'fear': 'eavesdroppers', 'source': 'wand',
            'robocop': 4, 'comments': 'Tell Bob I said hi.'},
            {'name': 'Bob', 'fear': 'bees', 'source': 'amulet', 'robocop': 4,
            'comments': 'n/a'},
            {'name': 'Carol', 'fear': 'puppets', 'source': 'crystal ball',
            'robocop': 1, 'comments': 'Please take the puppets out of the break room.'},
            {'name': 'Alex Murphy', 'fear': 'ED-209', 'source': 'money',
            'robocop': 5, 'comments': 'Protect the innocent. Serve the public trust. Uphold the law.'},]

for person in formData:
    ## Give the user a chance to kill the script
    print( ">>> 5 SECOND PAUSE TO LET THE USER ABORT BY PRESSING CTRL-C <<<" )
    time.sleep( 5 )
    
    ## Wait until the form page has loaded  !!! It seems not to work, no matter how hard I try
#    while not pyautogui.pixelMatchesColor( submitButton[ 0 ], submitButton[ 1 ], submitButtonColor ):
#        time.sleep( 0.5 )
    ## Fill out the Name field
    print( "Entering info for %s...." % ( person[ "name" ] ) )
    pyautogui.click( nameField[ 0 ], nameField[ 1 ] )
    
    pyautogui.typewrite( person[ "name" ] + "\t" )
    
    ## Fill out the greatest fear field
    pyautogui.typewrite( person[ "fear" ] + "\t" )
    
    ## Fill out the Source of WIzard Powers field
    if person[ "source" ] == "wand":
        pyautogui.typewrite( [ "down", "\t" ] )
    elif person[ "source" ] == "amulet":
        pyautogui.typewrite( [ "down", "down", "\t" ] )
    elif person[ "source" ] == "crystal ball":
        pyautogui.typewrite( [ "down", "down", "down", "\t" ] )
    elif person[ "source" ] == "money":
        pyautogui.typewrite( [ "down", "down", "down", "down", "\t" ] )
    
## Fill out the Robocop field
    if person[ "robocop" ] == 1:
        pyautogui.typewrite( [ " ", "\t" ] )
    elif person[ "robocop" ] == 2:
        pyautogui.typewrite( [ "right", "\t" ] )
    elif person[ "robocop" ] == 3:
        pyautogui.typewrite( [ "right", "right", "\t" ] )
    elif person[ "robocop" ] == 4:
        pyautogui.typewrite( [ "right", "right", "right", "\t" ] )
    elif person[ "robocop" ] == 5:
        pyautogui.typewrite( [ "right", "right", "right", "right", "\t" ] )
    
## Fill out the additional comments field
    pyautogui.typewrite( person[ "comments" ] + "\t" )
    
## Click Submit
    pyautogui.press( "enter" )
    
## Wait until form page has loaded
    print( "Clicked submit." )
    time.sleep( 5 )
## Click the "submit another response" link
    pyautogui.click( submitAnotherLink[ 0 ], submitAnotherLink[ 1 ] )
